#!/usr/bin/env python3

import setuptools

setuptools.setup(

    name = 'enterprise_flyby',

    version = '1.0',

    description = 'A Starship Enterprise Flyby',

    long_description = 'A curses-based animation of the Starship Enterprise NCC-1701 flying across the terminal from right to left.',

    url = 'https://bitbucket.org/snippets/jefe2000/yRzGM',

    author = 'Geoff Cleary',

    author_email = 'g_cleary@u.pacific.edu',

    license = 'Apache',

    classifiers = [
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console :: Curses',
        'Intented Audience :: Developers',
        'Topic :: Software Development :: Easter Eggs',
        'License :: OSI Approved :: Apache Software License',
        'Programming Language :: Python 2.7',
        'Programming Language :: Python 3',
        'Natural Language :: English',
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: POSIX :: Linux',
        'Operating System :: Unix'
    ],

    keywords = 'easter egg interface animation',

    packages = setuptools.find_packages( )

)
