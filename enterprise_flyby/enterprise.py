#   Copyright 2016 Geoff Cleary
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#!/usr/bin/env python3

import curses
import time

def EnterpriseFlyBy ( stdscr ):
    enterprise  = [
'                                     _ ___________________________=====_____',
'              __--__                / ||     =<==== NCC-1701 =======       /',
'___________---______---___________  \_||__________________________________/',
'\________________________________/           | |',
'             \______/  \__ .. :  \           | |',
'               `--\'       \_   :  \          | |',
'                         __-`------`-----____| |',
'                      \ |||_     .::. :      |_|--_',
'                      -)=|__ =<=======--      :. |_\\',
'                      / |||           __________---\'',
'                          ------------' ]
    #  The red stripes along the nacelle and body of the Enterprise
    #  along with their X coordinates.
    decal = [ [ '=<====', '=======', '=<=======--'],
              [ 45      , 61      ,  29           ] ]
              
    curses.init_pair(1, curses.COLOR_RED, curses.COLOR_BLACK)
    initY = 5
    
    ( Ymax, Xmax ) = stdscr.getmaxyx()
    
    #  Hide the cursor
    curses.curs_set ( 0 )
    
    
    #  Output the image of the enterprise at every location from right to left
    #  across the screen.  Start at the right edge and continue until the
    #  Enterprise has disappeared off the left edge.
    for i in reversed(range(-len(enterprise[1]),Xmax)):
        #  Clear the screen.
        stdscr.erase()
        
        Y = initY
        
        #  Draw the enterprise, line by line.  Or, rather, deck by deck.  :-)
        for deck in enterprise:
            #  Drawing routine for the case wherein the Enterprise is bleeding
            #  off the right edge of the screen.
            if ( Xmax - i < len(enterprise[1])):
                #  Draw the portion of the deck up to, but not beyond, the edge.
                stdscr.addstr ( Y, i, deck[0:Xmax - i] )
                
                #  Color the decals on decks 1 and 8.  Only draw the visible 
                #  slices of the decals:  up to, but not beyond, the screen 
                #  edge.
                if Y - initY == 1:
                    if i + decal[1][0] < Xmax:
                        stdscr.addstr ( Y, i+decal[1][0],
                                        decal[0][0][0:Xmax-(i+decal[1][0])],
                                        curses.color_pair(1) )
                    if i + decal[1][1] < Xmax:
                        stdscr.addstr ( Y, i+decal[1][1],
                                        decal[0][1][0:Xmax-(i+decal[1][1])],
                                        curses.color_pair(1) )
                elif Y - initY == 8:
                    if i + decal[1][2] < Xmax:
                        stdscr.addstr ( Y, i+decal[1][2],
                                        decal[0][2][0:Xmax-(i+decal[1][2])],
                                        curses.color_pair(1) )
            #  The drawing routines for case wherein the Enterprise is bleeding 
            #  off the left edge of the screen.
            elif (i < 0):
                #  Draw the still visible portion of the Enterprise.
                stdscr.addstr ( Y, 0, deck[abs(i):])
                
                #  Color the decals on decks 1 and 8.  Only draw the visible 
                #  slices of the decals:  only the portions visible from the 
                #  left edge inward.
                if Y - initY == 1:
                    if i+decal[1][0] >= 0:
                        stdscr.addstr ( Y, i+decal[1][0],
                                        decal[0][0], curses.color_pair(1) )
                    else:
                        stdscr.addstr ( Y, 0,
                                        decal[0][0][abs(i+decal[1][0]):],
                                        curses.color_pair(1) )
                    if i+decal[1][1] > 0:
                        stdscr.addstr ( Y, i+decal[1][1],
                                        decal[0][1], curses.color_pair(1) )
                    else:
                        stdscr.addstr ( Y, 0,
                                        decal[0][1][abs(i+decal[1][1]):],
                                        curses.color_pair(1) )
                elif Y - initY == 8:
                    if i+decal[1][2] > 0:
                        stdscr.addstr ( Y, i+decal[1][2],
                                        decal[0][2], curses.color_pair(1) )
                    else:
                        stdscr.addstr ( Y, 0,
                                        decal[0][2][abs(i+decal[1][2]):],
                                        curses.color_pair(1) )
            #  The drawing routines for the easiest case:  the entire Enterprise
            #  is visible, neither bleeding left nor right.
            else:
                #  Draw the entire deck
                stdscr.addstr ( Y, i, deck )
                
                #  Color the decals on decks 1 and 8
                if Y - initY == 1:
                    stdscr.addstr ( Y, i+decal[1][0],
                                    decal[0][0], curses.color_pair(1) )
                    stdscr.addstr ( Y, i+decal[1][1],
                                    decal[0][1], curses.color_pair(1) )
                elif Y - initY == 8:
                    stdscr.addstr ( Y, i+decal[1][2],
                                    decal[0][2], curses.color_pair(1) )
            
            #  Next line:  advance the Y coordinate by 1
            Y+=1
        
        stdscr.refresh()
        time.sleep(.02)
    
    return ( 0 )
# EnterpriseFlyBy

if __name__ == '__main__':
    curses.wrapper ( EnterpriseFlyBy )
